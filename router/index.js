const db = require("../models/alumnos.js");
const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();

module.exports = router;

//declarar array de objeto
alumno ={

}

router.post("/insertar", async (req, res) => {
    console.log('Petición recibida en la ruta /insertar');
    alumno = {
        matricula: req.body.matricula,
        nombre: req.body.nombre,
        domicilio: req.body.domicilio,
        sexo: req.body.sexo,
        especialidad: req.body.especialidad,
    };
    
    console.log(alumno);
     const resultado = await db.insertar(alumno);
    res.json(resultado);
  });
  

router.get("/", (req,res)=>{
  res.render('index.html');
})

router.get("/mostrarTodos", async (req, res) => {
  const resultado = await db.mostrarTodos();
  res.send(resultado);
});

router.delete('/borrar/:matricula', (req, res) => {
  const matricula = req.params.matricula;
  db.borrar(matricula)
    .then((data) => {
      if (data.deletedCount === 0) {
        res.status(404).send('No se encontró la matrícula');
      } else {
        res.json(data);
      }
    })
    .catch((err) => res.status(500).send(err.message));
});

router.get('/buscarMatricula/:matricula', function(req, res) {
  var matricula = req.params.matricula;
  db.buscarMatricula(matricula)
    .then((alumno) => {
      res.json(alumno);
    })
    .catch((error) => {
      console.error('error al buscar alumno:', error);
      res.status(500).send('error al buscar alumno');
    });
});

router.put('/actualizar/:matricula', function(req, res) {
  var matricula = req.params.matricula;
  var alumno = req.body;
  db.actualizar(matricula, alumno)
    .then((result) => {
      res.status(200).json({ message: result });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ message: 'Error al actualizar el alumno' });
    });
});

module.exports = router;