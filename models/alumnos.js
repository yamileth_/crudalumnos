const  json = require('express/lib/response');
const { connect, query } = require('../models/conexion.js');
const promise =  require('../models/conexion.js');
const conexion = require('../models/conexion.js'); 
const router = require('../router/index.js');
const {Route} = require('router');
var alumnosDb = {};
module.exports = alumnosDb;

alumnosDb.insertar = function insertar(alumno){
    return new Promise((resolve,reject)=>{
        var sqlConsulta = "insert into alumnos set ?";
        conexion.query(sqlConsulta, alumno, function(err,res){
            if(err){
                console.log("surgió un error" + err.message);
                reject(err);
            }else{
                resolve({
                    matricula: alumno.matricula,
                    nombre: alumno.nombre,
                    domicilio: alumno.domicilio,
                    sexo: alumno.sexo,
                    especialidad: alumno.especialidad
                })
            }
        })
    })
}

alumnosDb.mostrarTodos = function mostrarTodos() {
    return new Promise((resolve, reject) => {
      var sqlConsulta = "select * from alumnos";
      conexion.query(sqlConsulta, null, function (err, res) {
        if (err) {
          console.log("Surgió un error " + err.message);
          reject(err);
        } else {
          resolve(res);
        }
      });
    });
  };

//buscar alumno por matricula
alumnosDb.buscarMatricula = function buscarMatricula(matricula){
    return new Promise((resolve,reject)=>{
      var sqlConsulta = "SELECT * FROM alumnos WHERE matricula = ?";
      conexion.query(sqlConsulta, [matricula], function(err, res){
        if(err){
          console.log("Surgió un error " + err.message);
          reject(err);
        } else {
          if(res.length > 0){
            resolve(res[0]); // Devuelve solo el primer resultado
          } else {
            reject(new Error(`No se encontró un alumno con matrícula ${matricula}`));
          }
        }
      });
    });
  }

//borrar el alumno 
alumnosDb.borrar = function borrar(matricula) {
    return new Promise((resolve, reject) => {
      alumnosDb.buscarMatricula(matricula)
        .then(alumno => {
          conexion.query('DELETE FROM alumnos WHERE matricula = ?', [matricula], function(err, res) {
            if (err) {
              console.log('Surgió un error ' + err.message);
              reject(err);
            } else {
              resolve({ message: `User where matricula = ${matricula} deleted succesfully.` });
            }
          });
        })
        .catch(err => reject(err));
    });
  };

  alumnosDb.actualizar = function(matricula, alumno) {
    return new Promise((resolve, reject) => {
      const sql = 'UPDATE alumnos SET nombre = ?, domicilio = ?, sexo = ?, especialidad = ? WHERE matricula = ?';
      const params = [alumno.nombre, alumno.domicilio, alumno.sexo, alumno.especialidad, matricula];
      conexion.query(sql, params, function(err, result) {
        if (err) {
          reject(err);
        } else {
          resolve(`Alumno con matrícula ${matricula} actualizado correctamente.`);
        }
      });
    });
  };
  
// metodo put 
// router.put('/actualizar/:matricula', function(req, res) {
//   var matricula = req.params.matricula;
//   var alumno = req.body;
//   AlumnosDb.actualizar(matricula, alumno)
//     .then(() => {
        
//       res.status(200).json({ message: 'Modificado con éxito' });
//     })
//     .catch(err => {
//       console.log('Error al actualizar el alumno: ', err.message);
//       res.status(500).json({ message: 'Error al actualizar el alumno' });
//     });
// });

// // Modificar y actualizar alumno
// AlumnosDb.actualizar = function(matricula, alumno) {
//   return new Promise((resolve, reject) => {
//     const sql = 'UPDATE alumno SET nombre = ?, domicilio = ?, sexo = ?, especialidad = ? WHERE matricula = ?';
//     const params = [alumno.nombre, alumno.domicilio, alumno.sexo, alumno.especialidad, matricula];
//     conexion.query(sql, params, function(err, res) {
//       if (err) {
//         reject(err);
//       } else {
//         resolve(`Alumno con matrícula ${matricula} actualizado correctamente.`);
//       }
//     });
//   });
// };